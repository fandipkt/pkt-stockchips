<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pool;
use App\User;
class KolamController extends Controller
{
    public function index(){
        $data['pool'] = Pool::select('pools.id','pools.pool_name','pools.pool_capacity','users.lokasi')->join('users','pools.user_id','=','users.id')->get();
        $data['user'] = User::where('lokasi','<>','ADMIN')->get();
        return view('admin.kolam.index',compact('data'));
    }

    public function store(Request $request){
        $pool = new Pool;
        $pool->pool_name = $request->pool_name;
        $pool->pool_capacity = $request->pool_capacity;
        $pool->pool_capacity = $request->pool_capacity;
        $pool->user_id = $request->user_id;
        $pool->created_at = date('Y-m-d');
        if($pool->save()){
            $request->session()->flash('success', 'Kolam berhasil ditambahkan');
            return redirect()->back();
        }
        return redirect()->back();
    }


    public function show(Request $request,$id){
        if($request->ajax()){
            $pool = Pool::find($id);
            return response()->json(['data' => $pool]);
        }
    }

    public function update(Request $request){
        $pool = Pool::where('id',$request->hidden_id)->update([
            'pool_name'         => $request->nama_kolam,
            'pool_capacity'     => $request->kapasitas_kolam,
        ]);
        $request->session()->flash('success', 'Data kolam berhasil diperbaharui');
        return redirect()->back();
        
    }
}
