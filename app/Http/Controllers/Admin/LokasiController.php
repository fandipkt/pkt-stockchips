<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class LokasiController extends Controller
{
    public function index(){
        $data['user'] = User::where('lokasi','!=','ADMIN')->get();
        return view('admin.lokasi.index',compact('data'));
    }


}
