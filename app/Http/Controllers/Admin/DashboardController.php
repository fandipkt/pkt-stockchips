<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Karung;
use App\Obat;
use App\User;
use App\Order;
use App\Produksi;
use App\MediaTanam;
use App\Benang;
use DB;
class DashboardController extends Controller
{
    public function index(){
        $users = User::all();
        $mt_n_t  = Order::select(DB::raw('SUM(belum_diterima) as jumlah'))
                            ->where('produk','mt')
                            ->whereNull('finish_at')
                            ->groupBy('produk')
                            ->get();
        $obat_n_t = Order::select(DB::raw('SUM(belum_diterima) as jumlah'))
                            ->where('produk','obat')
                            ->whereNull('finish_at')
                            ->groupBy('produk')
                            ->get();
        $karung_n_t = Order::select(DB::raw('SUM(belum_diterima) as jumlah'))
                            ->where('produk','karung')
                            ->whereNull('finish_at')
                            ->groupBy('produk')
                            ->get();
        $chips_arr = array(); $mt_arr = array();$obat_arr = array();$karung_arr = array();$benang_arr = array();

        foreach($users as $key => $users){
            $chips = Produksi::where('user_id',$users->id)
                                ->orderBy('id','desc')
                                ->limit(1)->get();
            $mt     = MediaTanam::where('user_id',$users->id)
                            ->orderBy('id','desc')
                            ->limit(1)->get();
            $obat   = Obat::where('user_id',$users->id)
                            ->orderBy('id','desc')
                            ->limit(1)->get();
            $karung = Karung::where('user_id',$users->id)
                            ->orderBy('id','desc')
                            ->limit(1)->get();
            $benang = Benang::where('user_id',$users->id)
                            ->orderBy('id','desc')
                            ->limit(1)->get();
            $chips_arr[] = $chips;
            $mt_arr[] = $mt;
            $obat_arr[] =  $obat;
            $karung_arr[] =  $karung;
            $benang_arr[] =  $benang;
        }
        $chips_t = 0;
        foreach($chips_arr as $key => $chipt){
            $chips_t += (count($chipt) == 0) ? 0 : $chipt[0]->stock;
        }
        $mt_t = 0;
        foreach($mt_arr as $key => $mtt){
            $mt_t += (count($mtt) == 0) ? 0 : $mtt[0]->stock;
        }
        $obat_t = 0;
        foreach($obat_arr as $key => $ott){
            $obat_t += (count($ott) == 0) ? 0 : $ott[0]->stock;
        }
        $karung_t = 0;
        foreach($karung_arr as $key => $ktt){
            $karung_t += (count($ktt) == 0) ? 0 : $ktt[0]->stock;
        }
        $benang_t = 0;
        foreach($benang_arr as $key => $btt){
            $benang_t += (count($btt) == 0) ? 0 : $btt[0]->stock;
        }
        $user1 = DB::table('users')->get();
        return view('admin.dashboard.index',[
            'users' => $user1,
            'chips' => $chips_arr,
            'mt' => $mt_arr,
            'obat' => $obat_arr,
            'karung' => $karung_arr,
            'benang' => $benang_arr,
            'chips_t' => $chips_t,
            'mt_t' => $mt_t,
            'obat_t' => $obat_t,
            'karung_t' => $karung_t,
            'benang_t' => $benang_t,
            'belum_terima_mt' => $mt_n_t,
            'belum_terima_obat' => $obat_n_t,
            'belum_terima_karung' => $karung_n_t,
        ]);
    }
}
