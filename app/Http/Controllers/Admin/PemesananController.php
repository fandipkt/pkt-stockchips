<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\User;
use App\Order;
use App\OrderTemp;

class PemesananController extends Controller
{
    public function index(){
        $data['loc'] = User::where('lokasi','<>','ADMIN')->get();
        $data['pesan'] = Order::select('orders.id','orders.kode_pemesanan','orders.jumlah','orders.belum_diterima','users.lokasi')->where('finish_at',NULL)->join('users','orders.user_id','=','users.id')->get();
        $data['history'] = Order::whereNotNull('finish_at')->join('users','orders.user_id','=','users.id')->get();;
        return view('admin.pemesanan.index',compact('data'));
    }
    public function store(Request $request){
        $order = new Order;
        $order->produk = $request->produk;
        $order->kode_pemesanan = $request->kode_pemesanan;
        $order->jumlah = $request->jumlah;
        $order->belum_diterima = $request->jumlah;
        $order->user_id     = $request->user_id;
        $order->created_at = date('Y-m-d');
        if($order->save()){
            $request->session()->flash('success', 'Data Pemesanan Berhasil Dibuat');
            return redirect('admin/pemesanan');
        }
        $request->session()->flash('error', 'Gagal Membuat Pemesanan');
        return redirect('admin/pemesanan');
    }

    public function view($id){
        $order = Order::where('id',$id)->first();
        $terima = OrderTemp::where('order_id',$id)->get();
        return view('admin.pemesanan.detail',[
            'id' => $order,
            'terima' => $terima
        ]);
    }

    public function update(Request $request,$id){
        $now = Order::where('id',$id)->first();
        $acc = OrderTemp::select(DB::raw('SUM(jumlah) as terima'))->where('id',$id)->get();
        $new = $request->jumlah - $acc[0]->terima;
        $update = Order::where('id', $id)->update([
            'kode_pemesanan' => $request->kode_pemesanan,
            'jumlah' => $request->jumlah,
            'belum_diterima' => $new,
        ]);
        $request->session()->flash('success', 'Data Pemesanan Berhasil Diperbaharui');
         return redirect()->back();
    }

    public function finish(Request $request, $id){
        $date = date('Y-m-d');
        $id = Order::where('id',$id)->update([
            'finish_at' => $date
        ]);
        $request->session()->flash('success', 'Pemesanan telah diselesaikan.');
        return redirect()->back();
    }
}
