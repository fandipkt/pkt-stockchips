<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Obat;
class ObatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['obat'] = Obat::where('user_id',Auth::user()->id)->get();
        return view('obat.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data['obat'] = Obat::find($id);
        return view('obat.detail',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $obat = Obat::where('id',$id)->update([
            'jumlah' => $request->jumlah,
            'desc'   => $request->desc,
            'stock'  => Self::check_obat_by_id($id)[0]->stock + $request->jumlah,
        ]);
        
        if($id == Self::check_obat()[0]->id){
            $update = Obat::where('user_id',Auth::user()->id)->orderBy('id','desc')->limit(1)->update([
                'stock' => Self::check_obat()[0]->stock
            ]);    
        }
        else{
            $update = Obat::where('user_id',Auth::user()->id)->orderBy('id','desc')->limit(1)->update([
                'stock' => Self::check_obat()[0]->stock + $request->jumlah
            ]);
        }

        $request->session()->flash('success', 'Data Berhasil Diperbaharui!');
        return redirect('/obat');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function filter(Request $request){
        $from  = $request->get('from');
        $to    = $request->get('to');
        $data['obat'] = Obat::where('user_id',Auth::user()->id)->whereBetween('created_at',[$from, $to])->get();
        return view('obat.filter',compact('data'));
    }

    private function check_obat_by_id($id){
        $obat = Obat::where('id',$id)->where('user_id',Auth::user()->id)->get();
        return $obat;
    }

    private function check_obat(){
        $obat = Obat::where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $obat;    
    }
    private function check_obat_by_date($date){
        $obat = Obat::where('created_at',$date)
                ->where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $obat;
    }

}
