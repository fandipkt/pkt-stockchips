<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Benang;
class BenangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['benang'] = Benang::where('user_id',Auth::user()->id)->get();

        dd(count(Self::check_benang()));
        return view('benang.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data['benang'] = Benang::find($id);
        return view('benang.detail',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $benang = Benang::where('id',$id)->update([
            'jumlah' => $request->jumlah,
            'desc'   => $request->desc,
            'stock'  => Self::check_benang_by_id($id)[0]->stock + $request->jumlah,
        ]);
        
        if($id == Self::check_benang()[0]->id){
            $update = Benang::where('user_id',Auth::user()->id)->orderBy('id','desc')->limit(1)->update([
                'stock' => Self::check_benang()[0]->stock
            ]);    
        }
        else{
            $update = Benang::where('user_id',Auth::user()->id)->orderBy('id','desc')->limit(1)->update([
                'stock' => Self::check_benang()[0]->stock + $request->jumlah
            ]);
        }
        $request->session()->flash('success', 'Data Berhasil Diperbaharui!');
        return redirect('/benang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function check_benang_by_id($id){
        $benang = Benang::where('id',$id)->where('user_id',Auth::user()->id)->get();
        return $benang;
    }

    private function check_benang(){
        $benang = Benang::where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $benang;    
    }

    public function filter(Request $request){
        $from  = $request->get('from');
        $to    = $request->get('to');
        $data['benang'] = Benang::where('user_id',Auth::user()->id)->whereBetween('created_at',[$from, $to])->get();
        return view('benang.filter',compact('data'));
    }
}
