<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Order;
use App\OrderTemp;
use App\MediaTanam;
use App\Obat;
use App\Karung;
class PemesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pesan'] = Order::where('user_id',Auth::user()->id)->where('finish_at', NULL)->get();
        $data['history'] = Order::where('user_id',Auth::user()->id)->whereNotNull('finish_at')->get();
        return view('pemesanan.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $terima = OrderTemp::where('order_id',$id)->get();
        return view('pemesanan.detail',[
            'id' => $order,
            'terima' => $terima
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function accept(Request $request,$id){
        $date = date('Y-m-d');
        $order = Order::find($id);
        $terima = new OrderTemp;
        $terima->no_spb = $request->no_spb;
        $terima->no_polisi = $request->no_polisi;
        $terima->produk = $request->produk;
        $terima->jumlah = $request->jumlah;
        $terima->order_id = $request->pesanan_id;
        $terima->user_id = Auth::user()->id;
        $terima->created_at = $date;
        if($terima->save()){
            $order_up = Order::where('id',$id)->update([
                'belum_diterima' => $order->belum_diterima - $request->jumlah
            ]);
            if($request->produk == 'mt'){
                if(count(Self::check_mt_by_date()) == 0){
                    $mt = new MediaTanam;
                    $mt->terima = $request->jumlah;
                    $mt->stock = (count(Self::check_mt()) == 0) ? $request->jumlah : Self::check_mt()[0]->stock + $request->jumlah;
                    $mt->user_id = Auth::user()->id;
                    $mt->created_at = $date;
                    $mt->save();
                }
                else{
                    $mt = MediaTanam::where('created_at',$date)->where('user_id',Auth::user()->id)->update([
                        'terima'    => Self::check_mt_by_date()[0]->terima + $request->jumlah,
                        'stock'     => Self::check_mt_by_date()[0]->stock  + $request->jumlah
                    ]);
                }
            }
            else if($request->produk == 'obat'){
                if(count(Self::check_obat_by_date()) == 0){
                    $obat = new Obat;
                    $obat->terima = $request->jumlah;
                    $obat->stock = (count(Self::check_obat())==0) ? $request->jumlah : Self::check_obat()[0]->stock + $request->jumlah;
                    $obat->user_id = Auth::user()->id;
                    $obat->created_at = $date;
                    $obat->save();
                }
                else{
                    $obat = Obat::where('created_at',$date)->where('user_id',Auth::user()->id)->update([
                        'terima' => Self::check_obat_by_date()[0]->terima + $request->jumlah,
                        'stock'  => Self::check_obat_by_date()[0]->stock + $request->jumlah
                    ]);
                }
            }
            else{
                if(count(Self::check_karung_by_date()) == 0){
                    $karung = new Karung;
                    $karung->terima = $request->jumlah;
                    $karung->stock = (count(Self::check_karung() == 0)) ? $request->jumlah : Self::check_karung()[0]->stock + $request->jumlah;
                    $karung->user_id = Auth::user()->id;
                    $karung->created_at = $date;
                    $karung->save();
                }
                else{
                    $karung = Karung::where('created_at',$date)->where('user_id',Auth::user()->id)->update([
                        'terima' => Self::check_karung_by_date()[0]->terima + $request->jumlah,
                        'stock'  => Self::check_karung_by_date()[0]->stock + $request->jumlah
                    ]);
                }
            }
            $request->session()->flash('success', 'Pemesanan Berhasil ditambahkan');
            return redirect('/pemesanan');
        }
    }

    private function check_mt(){
        $mt = MediaTanam::where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $mt;    
    }

    private function check_mt_by_date(){
        $date = date('Y-m-d');
        $mt = MediaTanam::where('created_at',$date)
                ->where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $mt;
    }

    private function check_obat(){
        $obat = Obat::where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $obat;    
    }
    private function check_obat_by_date(){
        $date = date('Y-m-d');
        $obat = Obat::where('created_at',$date)
                ->where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $obat;
    }

    private function check_karung(){
        $karung = Karung::where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $karung;    
    }
    private function check_karung_by_date(){
        $date = date('Y-m-d');
        $karung = Karung::where('created_at',$date)
                ->where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $karung;
    }
}
