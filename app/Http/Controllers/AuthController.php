<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class AuthController extends Controller
{
    public function index(){
        if(Auth::guard('web')->check()){
            if(Auth::user()->role == 1){
                return redirect('admin/dashboard');
            }
            return redirect('/dashboard');
        }
        return view('login');
    }

    public function postLogin(Request $request){
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);
        $credential = $request->only('username', 'password');
        if(Auth::guard('web')->attempt($credential)){
            if(Auth::user()->role  == 1){
                return redirect('admin/dashboard');
            }
            else{
                return redirect('dashboard');
            }
        }
        else{
            $request->session()->flash('error', 'Username atau Password Salah!');
            $request->session()->flash('error1', 'Ulangi Kembali!');
            return redirect('/');
        }
    }
    public function logout(){
        Auth::guard('web')->logout();
    	return redirect('/');
    }

}
