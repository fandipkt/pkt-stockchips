<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Produksi;
use App\ProduksiTemp;
class ChipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Produksi::where('user_id',Auth::user()->id)->get();
        return view('chips.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $produksi = Produksi::find($id);
        return view('chips.detail',compact('produksi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $chips = Produksi::where('id',$id)->update([
            'jumlah' => $request->jumlah,
            'desc'   => $request->desc,
            'stock'  => Self::check_produksi_by_id($id)[0]->stock + $request->jumlah,
        ]);
        
        if($id == Self::check_produksi()[0]->id){
            $update = Produksi::where('user_id',Auth::user()->id)->orderBy('id','desc')->limit(1)->update([
                'stock' => Self::check_produksi()[0]->stock
            ]);    
        }
        else{
            $update = Produksi::where('user_id',Auth::user()->id)->orderBy('id','desc')->limit(1)->update([
                'stock' => Self::check_produksi()[0]->stock + $request->jumlah
            ]);
        }
        
        $request->session()->flash('success', 'Data Berhasil Diperbaharui!');
        return redirect('/chips');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function accept(Request $request,$id){
        $from  = ProduksiTemp::join('users','produksi_temps.user_id','=','users.id')->where('produksi_temps.id',$id)->get();
        if($from[0]->lokasi == 'GRIYA'){
            if(count(Self::check_produksi_by_date()) == 0){
                $produksi = new Produksi;
                $produksi->terima_griya = $from[0]->jumlah;
                $produksi->stock = Self::check_produksi()[0]->stock + $from[0]->jumlah;
                $produksi->user_id = Auth::user()->id;
                $produksi->created_at = date('Y-m-d');
                $produksi->save();
            }
            else{
                $produksi = Produksi::whereDate('created_at',date('Y-m-d'))->where('user_id',Auth::user()->id)->update([
                    'terima_griya' => Self::check_produksi()[0]->terima_griya + $from[0]->jumlah,
                    'stock' =>  Self::check_produksi()[0]->stock + $from[0]->jumlah,
                ]);
            }
        }
        else if($from[0]->lokasi == 'KIM'){
            if(count(Self::check_produksi_by_date()) == 0){
                $produksi = new Produksi;
                $produksi->terima_kim = $from[0]->jumlah;
                $produksi->stock = Self::check_produksi()[0]->stock + $from[0]->jumlah;
                $produksi->user_id = Auth::user()->id;
                $produksi->created_at = date('Y-m-d');
                $produksi->save();
            }
            else{
                $produksi = Produksi::whereDate('created_at',date('Y-m-d'))->where('user_id',Auth::user()->id)->update([
                    'terima_kim' => Self::check_produksi()[0]->terima_kim + $from[0]->jumlah,
                    'stock' =>  Self::check_produksi()[0]->stock + $from[0]->jumlah,
                ]);
            }
        }
        $produksiTemp = ProduksiTemp::where('id',$id)->update([
            'status' => 'Y'
        ]);
        $request->session()->flash('success', 'Chips Telah Diterima');
        return redirect('/dashboard');
    }

    private function check_produksi_by_id($id){
        $pr = Produksi::where('id',$id)->where('user_id',Auth::user()->id)->get();
        return $pr;
    }

    private function check_produksi(){
        $pr = Produksi::where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $pr;    
    }

    private function check_produksi_by_date(){
        $pr = Produksi::where('user_id',Auth::user()->id)->whereDate('created_at',date('Y-m-d'))
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $pr;    
    }

    public function filter(Request $request){
        $from  = $request->get('from');
        $to    = $request->get('to');
        $filter = Produksi::where('user_id',Auth::user()->id)->whereBetween('created_at',[$from, $to])->get();
        return view('chips.filter',compact('filter'));
    }
}
