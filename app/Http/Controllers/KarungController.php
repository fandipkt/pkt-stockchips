<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Karung;
class KarungController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['karung'] = Karung::where('user_id',Auth::user()->id)->get();
        return view('karung.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data['karung']= Karung::find($id);
        return view('karung.detail',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $karung = Karung::where('id',$id)->update([
            'jumlah' => $request->jumlah,
            'desc'   => $request->desc,
            'stock'  => Self::check_karung_by_id($id)[0]->stock + $request->jumlah,
        ]);
        
        if($id == Self::check_karung()[0]->id){
            $update = Karung::where('user_id',Auth::user()->id)->orderBy('id','desc')->limit(1)->update([
                'stock' => Self::check_karung()[0]->stock
            ]);    
        }
        else{
            $update = Karung::where('user_id',Auth::user()->id)->orderBy('id','desc')->limit(1)->update([
                'stock' => Self::check_karung()[0]->stock + $request->jumlah
            ]);
        }
        $request->session()->flash('success', 'Data Berhasil Diperbaharui!');
        return redirect('/karung');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function filter(Request $request){
        $from  = $request->get('from');
        $to    = $request->get('to');
        $data['karung'] = Karung::where('user_id',Auth::user()->id)->whereBetween('created_at',[$from, $to])->get();
        return view('karung.filter',compact('data'));
    }

    private function check_karung_by_id($id){
        $obat = Karung::where('id',$id)->where('user_id', Auth::user()->id)->get();
        return $obat;
    }

    private function check_karung(){
        $karung = Karung::where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $karung;    
    }
    private function check_karung_by_date($date){
        $karung = Karung::where('created_at',$date)
                ->where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $karung;
    }

}
