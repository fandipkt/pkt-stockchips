<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Produksi;
use App\ProduksiTemp;
class PengirimanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pengiriman'] = ProduksiTemp::where('user_id',Auth::user()->id)->get();
        return view('pengiriman.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = date('Y-m-d');
        $produksi_temp = new ProduksiTemp;
        $produksi_temp->tujuan = $request->tujuan;
        $produksi_temp->no_spb = $request->no_spb;
        $produksi_temp->no_polisi = $request->no_polisi;
        $produksi_temp->jumlah = $request->jumlah;
        $produksi_temp->status = 'N';
        $produksi_temp->user_id  = Auth::user()->id;
        $produksi_temp->created_at = $date;
        if($produksi_temp->save()){
            if($request->tujuan == 'jade'){
                if(count(Self::check_produksi_by_date()) == 0){
                    $produksi = new Produksi;
                    $produksi->keluar_jade = $request->jumlah;
                    $produksi->stock = Self::check_produksi()[0]->stock - $request->jumlah;
                    $produksi->user_id = Auth::user()->id;
                    $produksi->created_at = $date;
                    $produksi->save();
                }
                else{
                    $produksi = Produksi::whereDate('created_at',$date)->where('user_id',Auth::user()->id)->update([
                        'keluar_jade' => Self::check_produksi()[0]->keluar_jade + $request->jumlah,
                        'stock' =>  Self::check_produksi()[0]->stock - $request->jumlah,
                    ]);
                }
            }
            if($request->tujuan == 'gaperta'){
                if(count(Self::check_produksi_by_date()) == 0){
                    $produksi = new Produksi;
                    $produksi->keluar_gaperta = $request->jumlah;
                    $produksi->stock = Self::check_produksi()[0]->stock - $request->jumlah;
                    $produksi->user_id = Auth::user()->id;
                    $produksi->created_at = $date;
                    $produksi->save();
                }
                else{
                    $produksi = Produksi::whereDate('created_at',$date)->where('user_id',Auth::user()->id)->update([
                        'keluar_gaperta' => Self::check_produksi()[0]->keluar_gaperta + $request->jumlah,
                        'stock' =>  Self::check_produksi()[0]->stock - $request->jumlah,
                    ]);
                }
            }
            $request->session()->flash('success', 'Chips Berhasil Dikirim');
        }
        return redirect('/pengiriman');
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function check_produksi(){
        $pr = Produksi::where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $pr;    
    }

    private function check_produksi_by_date(){
        $pr = Produksi::where('user_id',Auth::user()->id)->whereDate('created_at',date('Y-m-d'))
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $pr;    
    }

    public function storeJade(Request $request){
        $produksiT = new ProduksiTemp;
        $produksi  = new Produksi;
        $produksiT->tujuan = $request->tujuan;
        $produksiT->no_spb = $request->no_spb;
        $produksiT->no_polisi = $request->no_polisi;
        $produksiT->jumlah = $request->jumlah;
        $produksiT->user_id = Auth::user()->id;
        $produksiT->created_at = date('Y-m-d');
        if(count(Self::check_produksi_by_date()) == 0){
            $produksi->keluar_penangkutan = $request->jumlah;
            $produksi->nama_pt = $request->nama_pt;
            $produksi->stock = Self::check_produksi()[0]->stock - $request->jumlah;
            $produksi->user_id  = Auth::user()->id;
            $produksi->created_at = date('Y-m-d');
        }
        else{
            $produksi = Produksi::whereDate('created_at',date('Y-m-d'))->where('user_id',Auth::user()->id)->update([
                'keluar_penangkutan' => Self::check_produksi()[0]->keluar_pengangkutan + $request->jumlah,
                'nama_pt'   => $request->nama_pt,
                'stock'     => Self::check_produksi()[0]->stock - $request->jumlah
            ]);
        }
        if($produksiT->save() && $produksi->save()){
            $request->session()->flash('success', 'Chips Berhasil Dikirim ke Pengangkutan');
            return redirect()->back();
        }
        $request->session()->flash('error', 'Chips Gagal Dikirim ke Pengangkutan');
        return redirect()->back();
    }

}
