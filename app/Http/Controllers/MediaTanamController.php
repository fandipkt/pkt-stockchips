<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\MediaTanam;
class MediaTanamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mt = MediaTanam::where('user_id',Auth::user()->id)->get();
        $last = Self::check_mt()[0]->stock;
        return view('mediatanam.index', compact('mt','last'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $mt = MediaTanam::find($id);
        return view('mediatanam.detail',compact('mt'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mt = MediaTanam::where('id',$id)->update([
            'jumlah' => $request->jumlah,
            'desc'   => $request->desc,
            'stock'  => Self::check_mt_by_id($id)[0]->stock + $request->jumlah,
        ]);
        
        if($id == Self::check_mt()[0]->id){
            $update = MediaTanam::where('user_id',Auth::user()->id)->orderBy('id','desc')->limit(1)->update([
                'stock' => Self::check_mt()[0]->stock
            ]);    
        }
        else{
            $update = MediaTanam::where('user_id',Auth::user()->id)->orderBy('id','desc')->limit(1)->update([
                'stock' => Self::check_mt()[0]->stock + $request->jumlah
            ]);
        }
        $request->session()->flash('success', 'Data Berhasil Diperbaharui!');
        return redirect('/mt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function filter(Request $request){
        $from   = $request->get('from');
        $to     = $request->get('to');
        $mt     = MediaTanam::where('user_id',Auth::user()->id)->whereBetween('created_at',[$from, $to])->get();
        return view('mediatanam.filter',compact('mt'));
    }

    private function check_mt_by_id($id){
        $mt = MediaTanam::where('id',$id)->where('user_id',Auth::user()->id)->get();
        return $mt;
    }

    private function check_mt(){
        $mt = MediaTanam::where('user_id', Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $mt;    
    }

    private function check_mt_by_date($date){
        $mt = MediaTanam::where('created_at',$date)
                ->where('user_id', Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $mt;
    }

}
