<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Pool;
use App\Batch;
use App\Activity;
use App\MediaTanam;
use App\Obat;
use App\Karung;
use App\Produksi;
use App\Benang;
use App\ProduksiTemp;

class DashboardController extends Controller
{
    public function index(){
        $data['notif']  = ProduksiTemp::where('status','N')
                            ->where('tujuan','<>','gaperta')
                            ->where('tujuan','<>','pengangkutan')
                            ->select('produksi_temps.id','produksi_temps.jumlah','users.lokasi')
                            ->join('users','produksi_temps.user_id','=','users.id')
                            ->get();
        $data['chips']  = Produksi::where('user_id',Auth::user()->id)->orderBy('id','desc')->limit(1)->get();
        $data['mt']     = MediaTanam::where('user_id',Auth::user()->id)->orderBy('id','desc')->limit(1)->get();
        $data['obat']   = Obat::where('user_id',Auth::user()->id)->orderBy('id','desc')->limit(1)->get();
        $data['karung'] = Karung::where('user_id',Auth::user()->id)->orderBy('id','desc')->limit(1)->get();
        $data['benang'] = Benang::where('user_id',Auth::user()->id)->orderBy('id','desc')->limit(1)->get();
        $pool = Pool::where('user_id',Auth::user()->id)->get();
        
        $html = '';
        if(count($pool) != 0){
            for($i = 1; $i<=count($pool); $i++){
                $pool_id        = $pool[$i-1]->id;
                $pool_name      = $pool[$i-1]->pool_name;
                $pool_capacity  = $pool[$i-1]->pool_capacity;
                $batch = Batch::where('done','N')
                        ->where('pool_id',$pool_id)
                        ->where('user_id',Auth::user()->id)
                        ->orderBy('id','DESC')
                        ->limit(1)
                        ->get();
                $act_btn = '';
                $batch_btn = '';
                if(count($batch) <= 0){
                    $batch_btn .= "<span class=\"badge badge-secondary mb-3\">Klik <b>Tambah Aktifitas</b> Untuk Membuat Batch Baru. </span>";
                    $act_btn .= "<a href=\"dashboard/create/activity/$pool_id\" class=\"btn btn-success btn-fw mb-3\">Tambah Aktifitas +</a>"; 
                }
                else{
                    $batch_no = $batch[0]->batch;
                    $batch_id = $batch[0]->id;
                    $batch_btn .= "<a href=\"dashboard/batch/$batch_id\" class=\"btn btn-info btn-fw mb-3\">Selesaikan Batch <b>{$batch_no}</b> </a>"; 
                    $act_btn .= "<a href=\"dashboard/create/activity/$pool_id\" class=\"btn btn-success btn-fw mb-3\">Tambah Aktifitas +</a>"; 
                }
                $activity_query = 
                Activity::select('activities.created_at',
                                'batches.batch',
                                'pools.pool_capacity',
                                'activities.sak_masuk',
                                'activities.kurang',
                                'activities.obat_masuk',
                                'activities.produksi_karung',
                                'activities.penggunaan_karung')
                        ->join('batches','activities.batch_id','=','batches.id')
                        ->join('pools','activities.pool_id','=','pools.id')
                        ->where('activities.pool_id',$pool_id)
                        ->where('pools.user_id',Auth::user()->id)
                        ->get();
                $html .= 
                "<div class=\"row\"><div class=\"col-lg-12 grid-margin stretch-card\">
                    <div class=\"card\">    
                        <div class=\"card-body\">
                            <div class=\"row\">
                                <div class=\"col-6\">
                                    <h4 class=\"card-title\">{$pool_name}</h4>
                                </div>
                                <div class=\"col-6 p-0 text-right\">
                                    {$batch_btn}
                                    {$act_btn}
                                </div>
                            </div>";
                $html .=  "<div class=\"table-responsive\"><table class=\"table display table-bordered\" id=\"table-mt\">
                                <thead>
                                    <tr>
                                        <th rowspan=\"2\">Tanggal</th>
                                        <th rowspan=\"2\">Batch</th>
                                        <th colspan=\"3\" class=\"text-center\">MT</th>  
                                        <th rowspan=\"2\">Obat Masuk</th>
                                        <th rowspan=\"2\">Produksi (Karung)</th>
                                    </tr>
                                    <tr>
                                        <th>Kapasitas</th>
                                        <th>Sak Masuk</th>
                                        <th>Kurang</th>
                                    </tr>
                                </thead>
                                <tbody>";
                                foreach($activity_query as $qq)
                                {                                    
                                    $html .="<tr>
                                    <td>{$this->date_indo($qq->created_at)}</td>
                                    <td>{$qq->batch}</td>
                                    <td>{$qq->pool_capacity}</td>
                                    <td>{$qq->sak_masuk}</td>
                                    <td>{$qq->kurang}</td>
                                    <td>{$qq->obat_masuk}</td>
                                    <td>{$qq->produksi_karung} ({$qq->penggunaan_karung})</td>
                                </tr>";
                                } 
                $html .= "</tbody></table></div></div></div></div></div>";
            }
        }
        $data['html'] = $html;
        return view('dashboard.index',compact('data'));
    }

    public function create($id){
        $pool   = Pool::where('user_id',Auth::user()->id)->where('id',$id)->first();
        $batch  = Batch::where('done',"N")
                ->where('active',"Y")
                ->where('user_id',Auth::user()->id)
                ->where('pool_id',$id)
                ->orderBy('id','DESC')
                ->limit(1)
                ->get();
        $data['pool'] = $pool;
        $data['batch'] = $batch;
        if(count($batch) <= 0){
            $last_batch = Batch::select('batch')->orderBy('id','DESC')->limit(1)->get();
            $mBatch = new Batch;
            $mBatch->batch = $last_batch[0]->batch + 1;
            $mBatch->active = "Y";
            $mBatch->done = "N";
            $mBatch->user_id = Auth::user()->id;
            $mBatch->pool_id = $id;
            $mBatch->created_at = date('Y-m-d');
            if($mBatch->save()){
                return redirect()->route('dashboard.create.activity', $id);
            }
        }
        return view('dashboard.create',compact('data'));
    }

    public function store(Request $request){
        $batch  = Batch::join('pools','pools.id','=','batches.pool_id')->where('batches.id',$request->batch_id)->get();
        $in     = Activity::where('batch_id',$request->batch_id)->get();
        $sum    = Activity::select(DB::raw('SUM(sak_masuk) as sak_masuk'))->where('batch_id',$request->batch_id)->get();

        $mAct = new Activity;
        $mAct->sak_masuk = $request->sak_masuk;
        $mAct->kurang = (count($in) <= 0 ) ? $batch[0]->pool_capacity - $request->sak_masuk : ($batch[0]->pool_capacity - $sum[0]->sak_masuk) - $request->sak_masuk;
        $mAct->obat_masuk = $request->obat_masuk;
        $mAct->produksi_karung = $request->produksi_karung;
        $mAct->penggunaan_karung = $request->penggunaan_karung;
        $mAct->batch_id = $request->batch_id;
        $mAct->pool_id = $request->pool_id;
        $mAct->created_at = $request->created_at;
        if($mAct->save()){
            if(count(Self::check_mt_by_date($request->created_at)) == 0){
                $mt = new MediaTanam;
                $mt->terima = 0;
                $mt->masuk_kolam = $request->sak_masuk;
                $mt->stock = $this->check_mt()[0]->stock - $request->sak_masuk;
                $mt->user_id = Auth::user()->id;
                $mt->created_at = $request->created_at;
                $mt->save();
            }
            else{
                $mt = MediaTanam::whereDate('created_at',$request->created_at)->where('user_id',Auth::user()->id)->update([
                    'terima' => $this->check_mt()[0]->terima,
                    'masuk_kolam' => $this->check_mt()[0]->masuk_kolam + $request->sak_masuk,
                    'stock' => $this->check_mt()[0]->stock - $request->sak_masuk
                ]);
            }
            if(count(Self::check_obat_by_date($request->created_at)) == 0){
                $obat = new Obat;
                $obat->terima = 0;
                $obat->digunakan = $request->obat_masuk;
                $obat->stock = $this->check_obat()[0]->stock - $request->obat_masuk;
                $obat->user_id = Auth::user()->id;
                $obat->created_at = $request->created_at;
                $obat->save();
            }
            else{
                $obat = Obat::whereDate('created_at',$request->created_at)->where('user_id',Auth::user()->id)->update([
                    'terima' => $this->check_obat()[0]->terima,
                    'digunakan' => $this->check_obat()[0]->digunakan + $request->obat_masuk,
                    'stock'     => $this->check_obat()[0]->stock - $request->obat_masuk
                ]);
            }
            if(count(Self::check_produksi_by_date($request->created_at)) == 0){
                $pr = new Produksi;
                $pr->produksi = $request->produksi_karung;
                $pr->stock = $this->check_produksi()[0]->stock + $request->produksi_karung;
                $pr->user_id = Auth::user()->id;
                $pr->created_at = $request->created_at;
                $pr->save();
            }
            else{
                $pr = Produksi::whereDate('created_at',$request->created_at)->where('user_id',Auth::user()->id)->update([
                    'produksi' => $this->check_produksi()[0]->produksi + $request->produksi_karung,
                    'stock' =>  $this->check_produksi()[0]->stock + $request->produksi_karung,
                ]);
            }
            if(count(Self::check_karung_by_date($request->created_at)) == 0){
                $karung = new Karung;
                $karung->terima = 0;
                $karung->digunakan = $request->penggunaan_karung;
                $karung->stock = $this->check_karung()[0]->stock - $request->penggunaan_karung;
                $karung->user_id = Auth::user()->id;
                $karung->created_at = $request->created_at;
                $karung->save();
            }
            else{
                $karung = Karung::whereDate('created_at',$request->created_at)->where('user_id',Auth::user()->id)->update([
                    'digunakan' => $this->check_karung()[0]->digunakan + $request->penggunaan_karung,
                    'stock' => $this->check_karung()[0]->stock - $request->penggunaan_karung
                ]);
            }
            $request->session()->flash('success', 'Aktifitas Kolam Berhasil Ditambahkan');
            return redirect('/dashboard');
        }
        $request->session()->flash('error', 'Gagal Menambahkan Aktifitas Kolam!');
        return redirect('/dashboard');
    }

    private function check_mt(){
        $mt = MediaTanam::whereDate('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $mt;    
    }

    private function check_mt_by_date($date){
        $mt = MediaTanam::whereDate('created_at',$date)
                ->where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $mt;
    }

    private function check_obat(){
        $obat = Obat::where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $obat;    
    }
    private function check_obat_by_date($date){
        $obat = Obat::whereDate('created_at',$date)
                ->where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $obat;
    }

    private function check_produksi(){
        $pr = Produksi::where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $pr;    
    }

    private function check_produksi_by_date($date){
        $pr = Produksi::whereDate('created_at',$date)
                ->where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $pr;
    }

    private function check_karung(){
        $karung = Karung::where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $karung;    
    }
    private function check_karung_by_date($date){
        $karung = Karung::whereDate('created_at',$date)
                ->where('user_id',Auth::user()->id)
                ->orderBy('id','desc')
                ->limit(1)->get();
        return $karung;
    }

    public function batch(Request $request, $id){
        $batch = Batch::where('id',$id)->update([
            'active' => 'N',
            'done'  => 'Y',
        ]);
        $request->session()->flash('success', 'Batch Berhasil Diselesaikan!');
        return redirect('/dashboard');
    }

    // private function date_indo($tgl){
    //     $ex = explode('-',$tgl);
    //     $tgl = $ex[2];
    //     $bln = $ex[1];
    //     $thn = $ex[0];
    //     $join = $tgl.'-'.$bln.'-'.$thn;
    //     return $join;
    // }
}
