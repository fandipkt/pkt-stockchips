<?php

namespace App\Http\Middleware;
use Auth;
use Closure;
use App\User;
class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where('role',Auth::user()->role)->first();
        if($request->is('admin/*')){
            if($user->role != 1){
                return abort(404);
            }
            return $next($request);
        }
        else{
            if($user->role == 1){
                return abort(404);
            }
            return $next($request);
        }
    }
}
