<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karung extends Model
{
    protected $table = 'karungs';
    protected $fillable = [
        'terima',
        'digunakan',
        'jumlah',
        'desc',
        'stock',
        'user_id',
        'created_at',
    ];
    public $timestamps = false;
}
