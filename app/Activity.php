<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'activities';
    protected $fillable = [
        'sak_masuk',
        'kurang',
        'obat_masuk',
        'produksi_karung',
        'penggunaan_karung',
        'batch_id',
        'pool_id',
        'user_id',
        'created_at',
    ];
    public $timestamps = false;
}
