<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pool extends Model
{
    protected $table = 'pools';
    protected $fillable = [
        'pool_name',
        'pool_capacity',
        'user_id',
        'created_at',
    ];
    public $timestamps = false;
}
