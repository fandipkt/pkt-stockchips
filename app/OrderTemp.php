<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderTemp extends Model
{
    protected $table = 'order_temps';
    protected $fillable = [
        'no_sbp','no_polisi','produk','jumlah','order_id','user_id','created_at'
    ];
    public $timestamps = false;
}
