<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Benang extends Model
{
    protected $table = 'benangs';
    protected $fillable = [
        'terima',
        'jumlah',
        'desc',
        'stock',
        'user_id',
        'created_at',
    ];
    public $timestamps = false;
}
