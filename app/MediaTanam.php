<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaTanam extends Model
{
    protected $table = 'media_tanams';
    protected $fillable = [
        'terima',
        'masuk_kolam',
        'jumlah',
        'desc',
        'stock',
        'user_id',
        'created_at',
    ];
    public $timestamps = false;
}
