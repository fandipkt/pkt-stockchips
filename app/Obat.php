<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Obat extends Model
{
    protected $table = 'obats';
    protected $fillable = [
        'terima',
        'digunakan',
        'jumlah',
        'desc',
        'stock',
        'user_id',
        'created_at',
    ];
    public $timestamps = false;
}
