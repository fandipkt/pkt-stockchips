<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $table = 'batches';
    protected $fillable = [
        'batch',
        'active',
        'done',
        'user_id',
        'pool_id',
        'created_at',
    ];
    public $timestamps = false;
}
