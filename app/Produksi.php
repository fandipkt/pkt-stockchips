<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produksi extends Model
{
    protected $table = 'produksis';
    protected $fillable = [
        'produksi',
        'keluar_jade',
        'keluar_gaperta',
        'kembali_jade',
        'terima_griya',
        'terima_kim',
        'kembali_griya',
        'keluar_gaperta_jade',
        'keluar_pengangkutan',
        'nama_pt',
        'stock',
        'user_id',
        'created_at',
    ];
    public $timestamps = false;
}
