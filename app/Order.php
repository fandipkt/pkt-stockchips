<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'produk',
        'kode_pemesanan',
        'jumlah',
        'finish_at',
        'created_at',
    ];
    public $timestamps = false;
}
