<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProduksiTemp extends Model
{
    protected $table = 'produksi_temps';
    protected $fillable = [
        'tujuan',
        'no_spb',
        'no_polisi',
        'jumlah',
        'status',
        'user_id',
        'created_at',
    ];
    public $timestamps = false;
}
