@extends('layout.master', 
            [
                'pageTitle'=>'Karung',
                'checkPoint'=> 'chips'
            ]
        )
@section('content')
@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif
<div class="row">
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <h4 class="card-title">Data karung</h4>
                </div>
            </div>
            <form action="{{route('karung.filter.date')}}" method="GET"> 
            <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Tanggal Awal</label>
                        <div class="input-group"> 
                            <div class="input-group-prepend"> 
                                <span class="input-group-text"><i class='fa fa-calendar'></i></span>
                            </div> 
                                <input type="text" class="form-control" id="awal" name="from" required autocomplete="off"> 
                        </div> 
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group"> 
                        <label>Tanggal Akhir</label>
                        <div class="input-group"> 
                            <div class="input-group-prepend"> 
                                <span class="input-group-text"><i class='fa fa-calendar'></i></span>
                            </div> 
                                <input type="text" class="form-control" id="akhir" name="to" required autocomplete="off"> 
                        </div> 
                    </div>
                </div>
                <div class="col-lg-3">
                    <br><br>
                    <button type="submit" class="btn btn-success mr-2"><i class='fa fa-search'></i></button>
                    <a href="{{route('karung')}}" class="btn btn-secondary mr-2">Reset</a>
                </div>
            </div>
            </div>
            </form>
            <div class="table-responsive">
                <table class="table table-striped" id="karung">
                <thead>
                    <tr>
                        <th rowspan="2">Tanggal</th>
                        <th rowspan="2">Terima</th>
                        <th rowspan="2">Digunakan</th>
                        <th colspan="2" class="text-center">DLL</th>  
                        <th rowspan="2">Stock</th>
                        <th rowspan="2">Info</th>
                    </tr>
                    <tr>
                        <th>Jumlah</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data['karung'] as $mts)
                        <tr>
                            <td>{{$mts->created_at}}</td>
                            <td>{{$mts->terima}}</td>
                            <td>{{$mts->digunakan}}</td>
                            <td>{{$mts->jumlah}}</td>
                            <td>{{$mts->desc}}</td>
                            <td>{{$mts->stock}}</td>
                            <td><a href="{{route('karung.detail', $mts->id)}}" class="btn btn-sm btn-info"><i class="icon-eye menu-icon"></i></a></td>
                        </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('script')
<script>
$(document).ready( function () {
    var table = $('#karung').DataTable({
          lengthChange: true,
          dom: 'Bfrtip',
          buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4,5,6],
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4,5,6 ]
                }
            },
            'colvis'
        ]
      });
      table.buttons().container().appendTo( $('.col-sm-6:eq(0)', table.table().container()));

        $( "#awal" ).datepicker({
                dateFormat: 'yy-mm-dd'
        });
        $( "#akhir" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });
});
</script>
@endsection