@extends('layout.master', 
            [
                'pageTitle'=>'Pemesanan',
                'checkPoint'=> 'pemesanan'
            ]
        )
@section('content')

@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <div class="row">
                <div class="col-6"> 
                    <h4 class="card-title mt-3">Pemesanan Produk </h4> 
                </div>
            </div>
            </p>
            <table class="table table-bordered" id="pemesanan_t_table">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> No Pemesanan</th>
                        <th> Produk </th>
                        <th> Jumlah (Sak)</th>
                        <th> Blm Diterima (Sak) </th>
                        <th> Action </th>
                    </tr>
                </thead>
                <tbody>
                @php
                    $no = 1;
                @endphp
                @foreach($data['pesan'] as $pesans)
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$pesans->kode_pemesanan}}</td>
                    <td>{{strtoupper($pesans->produk)}}</td>
                    <td>{{$pesans->jumlah}}</td>
                    <td>{{$pesans->belum_diterima}}</td>
                    <td>
                        <a href="{{route('pemesanan.view',$pesans->id)}}" class="btn btn-info btn-sm"><i class="mdi mdi-eye"></i></a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <div class="row">
                <div class="col-6"> <h4 class="card-title mt-3">History Pemesanan</h4> </div>
            </div>
            </p>
            <table class="table table-bordered" id="pemesanan_table">
                <thead>
                <tr>
                    <th>#</th>
                    <th> No Pemesanan </th>
                    <th> Jumlah </th>
                    <th> Tanggal Selesai </th>             
                </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($data['history'] as $datas)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{$datas->kode_pemesanan}}</td>
                            <td>{{$datas->jumlah}}</td>
                            <td>{{$datas->finish_at}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>


@endsection