@extends('layout.master', 
            [
                'pageTitle'=>'Pemesanan Detail',
                'checkPoint'=> 'pemesanan'
            ]
        )
@section('content')
@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif

<a href="{{route('pemesanan')}}" class="btn btn-secondary mt-1 mb-2">< Back</a>
<div class="row">
    <div class="col-lg-8 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="col-12">
                    <div class="row">
                        <div class="pl-0 col-6">
                            <h4 class="card-title">Rekap Pemesanan</h4>
                        </div>
                    </div>
                </div>
                <table class="mt-2 table table-bordered">
                    <thead>
                    <tr>
                        <th>Tgl</th>
                        <th>No Pemesanan</th>
                        <th>Produk</th>
                        <th>Jumlah Total</th>
                        <th>Belum Diterima</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$id->created_at}}</td>
                            <td>{{$id->kode_pemesanan}}</td>
                            <td>{{strtoupper($id->produk)}}</td>
                            <td>{{$id->jumlah}}</td>
                            <td>{{$id->belum_diterima}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-8 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="col-12">
                    <div class="row">
                        <div class="pl-0 col-6">
                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#produk-masuk">
                                Tambah Produk Masuk +
                            </button>
                        </div>
                    </div>
                </div>
                <table class="mt-4 table table-bordered">
                    <thead>
                    <tr>
                        <th>Tgl</th>
                        <th>No Polisi</th>
                        <th>No SBP</th>
                        <th>Jumlah</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($terima as $terimas)
                        <tr>
                            <td>{{$terimas->created_at}}</td>
                            <td>{{$terimas->no_polisi}}</td>
                            <td>{{$terimas->no_sbp}}</td>
                            <td>{{$terimas->jumlah}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="produk-masuk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Produk Masuk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('pemesanan.terima',$id->id)}}" method="POST">
            {{csrf_field()}}
                <input type="hidden" name="produk" value="{{$id->produk}}">
                <input type="hidden" name="no_spb" value="{{$id->kode_pemesanan}}">
                <input type="hidden" name="pesanan_id" value="{{$id->id}}">
                <div class="form-group">
                    <label>No Polisi</label>
                    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Nomor Polisi" name="no_polisi">
                </div> 
                <div class="form-group">
                    <label>Jumlah</label>
                    <input type="number" class="form-control" id="formGroupExampleInput" name="jumlah" placeholder="Jumlah">
                </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
  </div>
</div>


@endsection