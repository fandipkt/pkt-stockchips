@extends('layout_admin.master', 
            [
                'pageTitle'=>'Lokasi',
                'checkPoint' => 'lokasi'
            ]
        )
@section('content')
@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif
@if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <div class="row">
                <div class="col-6"> 
                    <h4 class="card-title mt-3">Data Lokasi </h4> 
                </div>
                <div class="col-6">
                    <div class="text-right">
                    </div>
                </div>
            </div>
            </p>
            <table class="table table-bordered" id="pemesanan_t_table">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> Username</th>
                        <th> Lokasi</th>
                        <th> Action </th>
                    </tr>
                </thead>
                <tbody>
                @php
                    $no = 1;
                @endphp
                @foreach($data['user'] as $pesans)
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$pesans->username}}</td>
                    <td>{{$pesans->lokasi}}</td>
                    <td>
                        <a href="#" class="btn btn-secondary btn-sm"><i class="mdi mdi-eye"></i></a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
@endsection