@extends('layout_admin.master', 
            [
                'pageTitle'=>'Pemesanan',
                'checkPoint' => 'pemesanan'
            ]
        )
@section('content')
@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif
@if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <div class="row">
                <div class="col-6"> 
                    <h4 class="card-title mt-3">Pemesanan Produk </h4> 
                </div>
                <div class="col-6">
                    <div class="text-right">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#tambah-pesanan">
                        Tambah Pemesanan
                        <i class="mdi mdi-plus menu-icon"></i>
                    </button>
                    </div>
                </div>
            </div>
            </p>
            <table class="table table-bordered" id="pemesanan_t_table">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> No Pemesanan</th>
                        <th> Jumlah (Sak)</th>
                        <th> Blm Diterima (Sak) </th>
                        <th> Lokasi Tujuan </th>
                        <th> Action </th>
                    </tr>
                </thead>
                <tbody>
                @php
                    $no = 1;
                @endphp
                @foreach($data['pesan'] as $pesans)
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$pesans->kode_pemesanan}}</td>
                    <td>{{$pesans->jumlah}}</td>
                    <td>{{$pesans->belum_diterima}}</td>
                    <td>{{$pesans->lokasi}}</td>
                    <td>
                        <a href="{{route('admin.pemesanan.view',$pesans->id)}}" class="btn btn-info btn-sm"><i class="mdi mdi-eye"></i></a>
                        <a href="{{route('admin.pemesanan.finish',$pesans->id)}}" class="btn btn-success btn-sm"><i class="mdi mdi-check"></i></a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah-pesanan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Pemesanan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('admin.pemesanan.store')}}" method="POST">
        {{csrf_field()}}
            <div class="form-group">
                <label for="">Produk</label>
                <select class="form-control form-control-lg" name="produk" required>
                    <option value="" disabled selected> -- Pilih Bahan -- </option>
                    <option value="mt"> MT </option>
                    <option value="obat"> Obat </option>
                    <option value="karung"> Karung </option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Lokasi Tujuan</label>
                <select class="form-control form-control-lg" name="user_id" required>
                    <option value="" disabled selected>-- Pilih Lokasi Tujuan --</option>
                    @foreach($data['loc'] as $locs)
                        <option value="{{$locs->id}}">{{$locs->lokasi}} </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Kode Pesanan</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Kode Pesanan" name="kode_pemesanan">
            </div>
            <div class="form-group">
                <label>Jumlah</label>
                <input type="number" class="form-control" id="formGroupExampleInput" name="jumlah" placeholder="Jumlah">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <div class="row">
                <div class="col-6"> 
                    <h4 class="card-title mt-3">History Pemesanan </h4> 
                </div>
            </div>
            </p>
            <table class="table table-bordered" id="history_t_table">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> No Pemesanan </th>
                        <th> Lokasi Tujuan </th>
                        <th> Jumlah (Sak)</th>
                        <th> Tgl Selesai </th>         
                    </tr>
                </thead>
                <tbody>
                @php
                    $no = 1;
                @endphp
                @foreach($data['history'] as $historis)
                    <tr>
                        <td>{{$no++}}</td>
                        <td>{{$historis->kode_pemesanan}}</td>
                        <td>{{$historis->lokasi}}</td>
                        <td>{{$historis->jumlah}}</td>
                        <td>{{$historis->finish_at}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
@endsection