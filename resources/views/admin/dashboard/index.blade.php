@extends('layout_admin.master', 
            [
                'pageTitle'=>'Dashboard',
                'checkPoint' => 'dashboard'
            ]
        )
@section('content')
<div class="row">
    <div class="col-lg-7 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <h4>Total</h4>
            <p class="card-description"></p>
                <table class="table table-hover table-bordered">                  
                    <tr>
                        <td></td>
                        <th>TOTAL</th>
                        @foreach($users as $user)
                            <th>{{ $user->lokasi }}</th>
                        @endforeach
                    </tr>
                <tr>
                    <th>CHIPS</th>
                    <td>{{$chips_t}}</td>
                    @foreach($chips as $chip)
                        <td>@if(count($chip) == 0)
                                0
                            @else 
                                {{$chip[0]->stock}}
                            @endif
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <th>MT</th>
                    <td>{{$mt_t}}</td>
                    @foreach($mt as $mts)
                        <td>@if(count($mts) == 0)
                                0
                            @else
                                {{$mts[0]->stock}}
                            @endif
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <th>Obat</th>
                    <td>{{$obat_t}}</td>
                    @foreach($obat as $mts)
                        <td>@if(count($mts) == 0)
                                0
                            @else
                                {{$mts[0]->stock}}
                            @endif
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <th>Karung</th>
                    <td>{{$karung_t}}</td>
                    @foreach($karung as $mts)
                        <td>@if(count($mts) == 0)
                                0
                            @else
                                {{$mts[0]->stock}}
                            @endif
                        </td>
                    @endforeach                
                </tr>
                <tr>
                    <th>Benang</th>
                    <td>{{$benang_t}}</td>
                    @foreach($benang as $mts)
                        <td>@if(count($mts) == 0)
                                0
                            @else
                                {{$mts[0]->stock}}
                            @endif
                        </td>
                    @endforeach
                </tr>
                </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-5 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <h4>Total Pesanan Yang Belum Diterima</h4>
            <p class="card-description"></p>
            <table class="table table-hover table-bordered">
                <tr>
                    <th>Produk</th>
                    <th>Total</th>
                </tr>
                <tr>
                    <td>MT</td>
                    <td>
                        @if(count($belum_terima_mt) == 0)
                            0
                        @else
                            {{$belum_terima_mt[0]->jumlah}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Obat</td>
                    <td>
                        @if(count($belum_terima_obat) == 0)
                            0
                        @else
                            {{$belum_terima_obat[0]->jumlah}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Karung</td>
                    <td>
                        @if(count($belum_terima_karung) == 0)
                            0
                        @else
                            {{$belum_terima_karung[0]->jumlah}}
                        @endif
                    </td>
                </tr>
            </table>
            </div>
        </div>
    </div>
</div>
@endsection