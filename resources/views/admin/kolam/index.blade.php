@extends('layout_admin.master', 
            [
                'pageTitle'=>'Kolam',
                'checkPoint' => 'kolam'
            ]
        )
@section('content')

@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif
@if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <div class="row">
                <div class="col-6"> 
                    <h4 class="card-title mt-3">Data Lokasi </h4> 
                </div>
                <div class="col-6">
                    <div class="text-right">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Tambah Kolam + </button>
                    </div>
                </div>
            </div>
            </p>
            <table class="table table-bordered" id="pemesanan_t_table">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> Nama Kolam</th>
                        <th> Kapasitas</th>
                        <th> Lokasi</th>
                        <th> Action </th>
                    </tr>
                </thead>
                <tbody>
                @php
                    $no = 1;
                @endphp
                @foreach($data['pool'] as $pesans)
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$pesans->pool_name}}</td>
                    <td>{{$pesans->pool_capacity}}</td>
                    <td>{{$pesans->lokasi}}</td>
                    <td>
                        <button type="button" name="edit" id="{{$pesans->id}}" class="edit btn-outline-success btn-sm"><i class="mdi mdi-pencil"></i></button>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

<div id="formModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <h5 class="modal-title">Edit Data Kolam</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
         <span id="form_result"></span>
         <form method="POST" id="form-kolam" class="form-horizontal" action="{{route('admin.kolam.update')}}">
          {{csrf_field()}}
          <div class="form-group">
                <label class="control-label col-md-4"> Nama Kolam : </label>
                <div class="col-md-12">
                    <input type="text" name="nama_kolam" id="nama_kolam" class="form-control" />
                </div>
           </div>
           <div class="form-group">
            <label class="control-label col-md-4"> Kapasitas : </label>
            <div class="col-md-12">
                <input type="text" name="kapasitas_kolam" id="kapasitas_kolam" class="form-control" />
            </div>
           </div>
           <br />
           <div class="form-group" align="center">
            <input type="hidden" name="action" id="action"/>
            <input type="hidden" name="hidden_id" id="hidden_id"/>
            <input type="submit" name="action_button" id="action_button" class="btn btn-success" value="Add"/>
           </div>
         </form>
        </div>
     </div>
    </div>
</div>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Kolam</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('admin.kolam.store')}}" method="POST">
        {{csrf_field()}}

            <div class="form-group">
                <label class="control-label col-md-4"> Nama Kolam : </label>
                <div class="col-md-12">
                    <input type="text" name="pool_name" id="nama_kolam" class="form-control" required/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4"> Kapasitas : </label>
                <div class="col-md-12">
                    <input type="number" name="pool_capacity" id="kapasitas_kolam" class="form-control"required/>
                </div>
            </div>
            <div class="form-group">
                <label for="">Lokasi</label>
                    <select class="form-control form-control-lg" name="user_id" required>
                        @foreach($data['user'] as $user)
                        <option value="{{$user->id}}"> {{$user->lokasi}} </option>
                        @endforeach
                    </select>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
@section('script')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.edit').click(function(){
        var id = $(this).attr('id');
        $('#form-result').html('');
        $.ajax({
            url : 'kolam/' + id,
            dataType: 'JSON',
            success: function(html){
                $('#nama_kolam').val(html.data.pool_name);
                $('#kapasitas_kolam').val(html.data.pool_capacity);
                $('#hidden_id').val(html.data.id);
                $('#action_button').val("Update");
                $('#formModal').modal('show');
            }
        })
    });

    // $('#form-kolam').on('submit', function(){
    //     if($('#action-button').val() == 'Update'){
    //         $.ajax({
    //             url : "{{ route('admin.kolam.update')}}",
    //             method: 'POST',
    //             data : new FormData(this),
    //             contentType: false,
    //             cache: false,
    //             processData : false,
    //             dataType : 'JSON',
    //             success: function (data){
    //                 if(data.success){
    //                     console.log(data);
    //                     html = '<div class="alert alert-success">' + data.success + '</div>';
    //                     $('#form-kolam')[0].reset();
    //                     $('#pemesanan_t_table').reload();
    //                 }
    //                 $('#form_result').html(html);
    //             }
    //         })
    //     }
    // });


</script>
@endsection