@extends('layout.master', 
            [
                'pageTitle'=>'Pengiriman',
                'checkPoint'=> 'pengiriman'
            ]
        )
@section('content')

@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <div class="row">
                <div class="col-6"> <h4 class="card-title mt-3">Pengiriman</h4></div>
                <div class="col-6">
                    <div class="text-right">
                    @if(Auth::user()->role == 3)
                        <button type="button" class="btn btn-success mb-3" data-toggle="modal" data-target="#kirim_jade">
                            Kirim Ke Pengangkutan
                        </button>
                    @else
                        <button type="button" class="btn btn-success mb-3" data-toggle="modal" data-target="#kirim">
                            Kirim Ke Lokasi
                        </button>
                    @endif
                    </div>
                </div>
            </div>

            <table class="table table-bordered" id="pengiriman">
                <thead>
                <tr>
                    <th> # </th>
                    <th> Tanggal </th>
                    <th> Tujuan </th>
                    <th> No Polisi </th>
                    <th> No SPB </th>
                    <th> Jumlah </th>
                    <th> Status </th>          
                </tr>
                </thead>
                <tbody>
                @php
                    $no = 1;
                @endphp
                @foreach($data['pengiriman'] as $data)
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$data->created_at}}</td>
                    <td>{{$data->tujuan}}</td>

                    <td>{{$data->no_polisi}}</td>
                    <td>{{$data->no_spb}}</td>
                    <td>{{$data->jumlah}}</td>
                    <td>
                        @if($data->tujuan == 'gaperta')
                            <label class="badge badge-secondary">Gaperta</label>
                        @else
                            @if($data->status == 'N')
                                <label class="badge badge-danger">Pending</label>
                            @else
                                <label class="badge badge-success">Diterima</label>
                            @endif
                        @endif
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

{{--LOKASI--}}

<div class="modal fade" id="kirim" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Pengiriman</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('pengiriman.store')}}" method="POST">
            {{csrf_field()}}
            <div class="form-group">
                <label for="">Tujuan</label>
                    <select class="form-control form-control-lg" name="tujuan" required>
                        <option value="" disabled selected> -- Pilih Tujuan -- </option>
                        <option value="jade"> JADE </option>
                        <option value="gaperta"> GAPERTA </option>
                    </select>
            </div>
            <div class="form-group">
                <label>No SBP</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="No SPB" name="no_spb">
            </div>

            <div class="form-group">
                <label>No Polisi</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="No Polisi" name="no_polisi">
            </div>
            <div class="form-group">
                <label>Jumlah</label>
                <input type="number" class="form-control" id="formGroupExampleInput" name="jumlah" placeholder="Jumlah">
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Kirim</button>
            </form>
        </div>
    </div>
  </div>
</div>

{{--PENGANGKUTAN--}}

<div class="modal fade" id="kirim_jade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Pengiriman</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('pengiriman.store.jade')}}" method="POST">
            {{csrf_field()}}
            <input type="hidden" name="tujuan" value="pengangkutan">
            <div class="form-group">
                <label>No SPB</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="No SPB" name="no_spb">
            </div>
            
            <div class="form-group">
                <label>No Polisi</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="No Polisi" name="no_polisi">
            </div>
            
            <div class="form-group">
                <label>Pengangkutan</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Kebun Yang Dituju" name="nama_pt">
            </div>

            <div class="form-group">
                <label>Jumlah</label>
                <input type="number" class="form-control" id="formGroupExampleInput" name="jumlah" placeholder="Jumlah">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
  </div>
</div>

@endsection

@section('script')
<script>
$(document).ready( function () {
    $('#pengiriman').DataTable();
} );
</script>
@endsection