@extends('layout.master', 
            [
                'pageTitle'=>'Chips Detail',
                'checkPoint'=> 'chips'
            ]
        )
@section('content')
@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif
<div class="row">
    <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
        <div class="card-body">
            <h4 class="card-title">Chips Tgl. {{$produksi->created_at}}</h4>
            <form class="forms-sample" method="POST" action="{{route('chips.update',$produksi->id)}}">
            {{csrf_field()}}
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Jumlah</label>
                        <input type="number" class="form-control" name="jumlah" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Deskripsi</label>
                <textarea class="form-control" id="exampleTextarea1" rows="4" name="desc" required></textarea>
            </div>
            <button type="submit" class="btn btn-primary mr-2">Simpan</button>
            <a href="{{route('chips')}}" class="btn btn-light">Batal</a>
            </form>
        </div>
        </div>
    </div>
    </div>
@endsection