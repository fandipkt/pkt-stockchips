@extends('layout.master', 
            [
                'pageTitle'=>'Chips',
                'checkPoint'=> 'chips'
            ]
        )

@section('content')
@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif
<div class="row">
<div class="col-lg-12 grid-margin stretch-card">
<div class="card">
<div class="card-body">
    <div class="row">
        <div class="col-6">
            <h3 class="card-title">Data Chips</h3>
        </div>
    </div>
    <form action="{{route('chips.filter.date')}}" method="GET"> 
    <div class="col-lg-12">
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                <label>Tanggal Awal</label>
                <div class="input-group"> 
                    <div class="input-group-prepend"> 
                        <span class="input-group-text"><i class='fa fa-calendar'></i></span>
                    </div> 
                        <input type="text" class="form-control" id="awal" name="awal" required autocomplete="off"> 
                </div> 
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group"> 
                <label>Tanggal Akhir</label>
                <div class="input-group"> 
                    <div class="input-group-prepend"> 
                        <span class="input-group-text"><i class='fa fa-calendar'></i></span>
                    </div> 
                        <input type="text" class="form-control" id="akhir" name="akhir" required autocomplete="off"> 
                </div> 
            </div>
        </div>
        <div class="col-lg-3">
            <br><br>
            <button type="submit" class="btn btn-success mr-2"><i class='fa fa-search'></i></button>
            <a href="{{route('chips')}}" class="btn btn-secondary mr-2">Reset</a>

        </div>
    </div>
    </div>
    </form>
    
    <!-- <p class="card-description">Add class <code>.table-striped</code></p> -->
    @if(Auth::user()->role == 3)
    <div class="table-responsive">
    <table class="table table-striped" id="chips">
        <thead>
        <tr>
            <th rowspan="2">Tanggal</th>
            <th rowspan="2">Produksi</th>
            <th rowspan="2">Terima Griya</th>
            <th rowspan="2">Terima Kim</th>
            <th rowspan="2">Keluar Gaperta</th>
            <th rowspan="2">Pengangkutan</th>
            <th rowspan="2">Nama Kebun</th>
            <th colspan="2" class="text-center">DLL</th>  
            <th rowspan="2">Stock</th>
            <th rowspan="2">Info</th>
        </tr>
        <tr>
                <th>Jumlah</th>
                <th>Keterangan</th>
        </tr>
        </thead>
        <tbody>
            @foreach($data as $chips)
                <tr>
                    <td>{{$chips->created_at}}</td>
                    <td>{{$chips->produksi}}</td>
                    <td>{{$chips->terima_griya}}</td>
                    <td>{{$chips->terima_kim}}</td>
                    <td>{{$chips->keluar_gaperta_jade}}</td>
                    <td>{{$chips->keluar_penangkutan}}</td>
                    <td>{{$chips->nama_pt}}</td>
                    <td>{{$chips->jumlah}}</td>
                    <td>{{$chips->desc}}</td>
                    <td>{{$chips->stock}}</td>
                    <td><a href="{{route('chips.detail', $chips->id)}}" class="btn btn-sm btn-info"><i class="icon-eye menu-icon"></i></a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    @else
    <div class="table-responsive">
    <table class="table table-striped" id="chips">
        <thead>
        <tr>
            <th rowspan="2">Tanggal</th>
            <th rowspan="2">Produksi</th>
            <th rowspan="2">Keluar J</th>
            <th rowspan="2">Keluar G</th>
            <th rowspan="2">Kembali J</th>
            <th colspan="2" class="text-center">DLL</th>  
            <th rowspan="2">Stock</th>
            <th rowspan="2">Info</th>
        </tr>
        <tr>
                <th>Jumlah</th>
                <th>Keterangan</th>
        </tr>
        </thead>
        <tbody>
            @foreach($data as $chips)
                <tr>
                    <td>{{$chips->created_at}}</td>
                    <td>{{$chips->produksi}}</td>
                    <td>{{$chips->keluar_jade}}</td>
                    <td>{{$chips->keluar_gaperta}}</td>
                    <td>{{$chips->kembali_jade}}</td>
                    <td>{{$chips->jumlah}}</td>
                    <td>{{$chips->desc}}</td>
                    <td>{{$chips->stock}}</td>
                    <td><a href="{{route('chips.detail', $chips->id)}}" class="btn btn-sm btn-info"><i class="icon-eye menu-icon"></i></a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    @endif
</div>
</div>
</div>
</div>
@endsection
@section('script')
<script>
$(document).ready( function () {
    var table = $('#chips').DataTable({
         "aaSorting": [],
          lengthChange: true,
          dom: 'Bfrtip',
          buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4,5,6,7],
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4,5,6,7 ]
                }
            },
            'colvis'
        ]
      });
      table.buttons().container().appendTo( $('.col-sm-6:eq(0)', table.table().container()));
    $( "#awal" ).datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $( "#akhir" ).datepicker({
        dateFormat: 'yy-mm-dd'
    });
});

</script>
@endsection