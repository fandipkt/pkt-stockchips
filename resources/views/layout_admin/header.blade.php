<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{$title}} - Admin :: Sistem Stock</title>
<link rel="stylesheet" href="{{asset('assets/vendors/mdi/css/materialdesignicons.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/feather/feather.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/base/vendor.bundle.base.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/flag-icon-css/css/flag-icon.min.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/vendors/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/jquery-bar-rating/fontawesome-stars-o.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/jquery-bar-rating/fontawesome-stars.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

