<!DOCTYPE html>
<html lang="en">
<head>
  @component('layout_admin.header',['title'=> $pageTitle]) @endcomponent
</head>
<body>
    <div class="container-scroller">
    @component('layout_admin.navbar')@endcomponent
    <div class="container-fluid page-body-wrapper">
    @component('layout_admin.sidebar',['checkPoint'=>$checkPoint])@endcomponent
    <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-sm-12 mb-4 mb-xl-0">
              <h4 class="font-weight-bold text-dark">{{$pageTitle}}</h4>
              {{--<p class="font-weight-normal mb-2 text-muted">Administrator</p>--}}
            </div>
          </div>
          @yield('content')
        </div>
    @component('layout_admin.footer')@endcomponent
    </div>
</div>
</div>
@component('layout_admin.bottom-tool')@endcomponent
</body>
</html>
