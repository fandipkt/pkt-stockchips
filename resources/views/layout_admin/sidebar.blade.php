<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <div class="user-profile">
          <div class="user-image">
            <img src="https://images.assetsdelivery.com/compings_v2/alekseyvanin/alekseyvanin1704/alekseyvanin170402285.jpg">
          </div>
          <div class="user-name">
              {{Auth::user()->lokasi}}
          </div>
          <div class="user-designation" style="font-size: 10px;">
              Online
          </div>
        </div>
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link @if($checkPoint == 'dashboard') active @endif" href="{{route('admin.dashboard')}}">
              <i class="icon-box menu-icon"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link @if($checkPoint == 'lokasi') active @endif" href="{{route('admin.lokasi')}}">
              <i class="icon-target menu-icon"></i>
              <span class="menu-title">Lokasi</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link @if($checkPoint == 'kolam') active @endif" href="{{route('admin.kolam')}}">
              <i class="icon-disc menu-icon"></i>
              <span class="menu-title">Kolam</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link @if($checkPoint == 'pemesanan') active @endif" href="{{route('admin.pemesanan')}}">
              <i class="icon-clipboard menu-icon"></i>
              <span class="menu-title">Pemesanan</span>
            </a>
          </li>
        </ul>
      </nav>