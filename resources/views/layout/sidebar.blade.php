<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <div class="user-profile">
          <div class="user-image">
            <img src="https://images.assetsdelivery.com/compings_v2/alekseyvanin/alekseyvanin1704/alekseyvanin170402285.jpg">
          </div>
          <div class="user-name">
            {{Auth::user()->lokasi}}
          </div>
          <div class="user-designation" style="font-size: 10px;">
              Online
          </div>
        </div>
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link @if($checkPoint == 'dashboard') active @endif" href="{{route('dashboard')}}">
              <i class="icon-box menu-icon"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="icon-disc menu-icon"></i>
              <span class="menu-title">Stock</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse @if($checkPoint == 'chips' || $checkPoint == 'media_tanam' || $checkPoint == 'obat' || $checkPoint == 'karung' || $checkPoint == 'benang') show @endif" id="ui-basic">
              <ul class="nav flex-column sub-menu">
              <li class="nav-item"> <a class="nav-link" href="{{route('chips')}}">Chips</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{route('media_tanam')}}">Media Tanam</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{route('obat')}}">Obat</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{route('karung')}}">Karung</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{route('benang')}}">Benang</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link @if($checkPoint == 'pemesanan') active @endif" href="{{route('pemesanan')}}">
              <i class="icon-file menu-icon"></i>
              <span class="menu-title">Pemesanan</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link @if($checkPoint == 'pengiriman') active @endif" href="{{route('pengiriman')}}">
              <i class="icon-location menu-icon"></i>
              <span class="menu-title">Pengiriman</span>
            </a>
          </li>
        </ul>
      </nav>