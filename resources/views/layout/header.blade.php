<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{$title}} - Sistem Stock ({{Auth::user()->lokasi}})</title>
<link rel="stylesheet" href="{{asset('assets/vendors/mdi/css/materialdesignicons.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/feather/feather.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/base/vendor.bundle.base.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/flag-icon-css/css/flag-icon.min.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/vendors/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/jquery-bar-rating/fontawesome-stars-o.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/jquery-bar-rating/fontawesome-stars.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
{{--DATEPICKER--}}
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap4.min.css">






