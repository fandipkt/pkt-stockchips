@extends('layout.master', 
            [
                'pageTitle'=>'Tambah Aktifitas Kolam',
                'checkPoint'=> 'dashboard'
            ]
        )
@section('content')
<div class="row">
    <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
        <div class="card-body">
            <h4 class="card-title">{{$data['pool']->pool_name}}</h4>
            <h6><span class="badge badge-info">Kapasitas : {{$data['pool']->pool_capacity}}</span></h6>
            <h6><span class="badge badge-success">Batch : {{$data['batch'][0]->batch}}</span></h6>

            <form class="forms-sample" method="POST" action="{{route('dashboard.store.activity')}}">
            {{csrf_field()}}
            <input type="hidden" value="{{ $data['batch'][0]->id}}" name="batch_id">
            <input type="hidden" value="{{ $data['pool']->id}}" name="pool_id">
            <div class="form-group">
                    <label>Pilih Tanggal</label>
                    <input type="text" class="form-control" id="date" name="created_at" required autocomplete="off">
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Sak Masuk</label>
                        <input type="number" class="form-control" name="sak_masuk">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Obat Masuk</label>
                        <input type="number" class="form-control" name="obat_masuk">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Produksi</label>
                        <input type="number" class="form-control" name="produksi_karung">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Penggunaan Karung</label>
                        <input type="number" class="form-control" name="penggunaan_karung">
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary mr-2">Simpan</button>
            <a href="{{route('dashboard')}}" class="btn btn-light">Batal</a>
            </form>
        </div>
        </div>
    </div>
    </div>
@endsection
@section('script')
<script>
$( function() {
    $( "#date" ).datepicker({
        dateFormat: 'yy-mm-dd'
    });
  } );
</script>
@endsection