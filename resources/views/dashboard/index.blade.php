@extends('layout.master', 
            [
                'pageTitle'=>'Dashboard',
                'checkPoint'=> 'dashboard'
            ]
        )
@section('content')

@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif
@if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif
@if(Auth::user()->role == 3)
        @foreach($data['notif'] as $notif)
        <div class="row mb-2">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-title mt-3">
                                    Pengiriman Chips Sebanyak {{$notif->jumlah}} dari {{$notif->lokasi}}
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="float-right">
                                    <a href="{{route('chips.accept',$notif->id)}}" class="btn btn-info">Terima</a>
                                    <a href="" class="btn btn-secondary">Tolak</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
@endif

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <table class="table table-hover">
                <tr>
                    <th>CHIPS</th>
                    <td>
                        @if(count($data['chips']) == NULL)
                            <b>0</b>
                        @else
                            <b>{{$data['chips'][0]->stock}}<b>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>MT</th>
                    <td>
                        @if(count($data['mt']) == NULL)
                            <b>0</b>
                        @else
                           <b> {{$data['mt'][0]->stock}} </b>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Obat</th>
                    <td>@if(count($data['obat']) == NULL)
                            <b>0</b>
                        @else
                            <b>{{$data['obat'][0]->stock}}</b>
                        @endif</td>
                </tr>
                <tr>
                <th>Karung</th>
                <td>@if(count($data['karung']) == NULL)
                            <b>0</b>
                        @else
                            <b>{{$data['karung'][0]->stock}}</b>
                        @endif</td>
                </tr>
                <tr>
                    <th>Benang</th>
                    <td>@if(count($data['benang']) == NULL)
                            <b>0</b>
                        @else
                            <b>{{$data['benang'][0]->stock}}</b>
                        @endif</td>
                </tr>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="mb-4"></div>
{!!$data['html']!!}
@endsection
@section('script')
<script>
$(document).ready( function () {
    $('.display').DataTable();
} );
</script>

@endsection