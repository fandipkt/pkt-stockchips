<?php
Route::get('/','AuthController@index')->name('view.login');
Route::post('/login','AuthController@postLogin')->name('post.login');
Route::get('logout','AuthController@logout')->name('logout');

Route::group(['middleware' => ['auth', 'role']], function(){
    //Admin
    Route::group(['prefix'=> 'admin/'], function(){
        Route::get('dashboard','Admin\DashboardController@index')->name('admin.dashboard');
        Route::get('lokasi','Admin\LokasiController@index')->name('admin.lokasi');
        Route::get('kolam','Admin\KolamController@index')->name('admin.kolam');
        Route::post('kolam/create','Admin\KolamController@store')->name('admin.kolam.store');
        Route::get('kolam/{id}','Admin\KolamController@show')->name('admin.kolam.show');
        Route::post('kolam/update/','Admin\KolamController@update')->name('admin.kolam.update');

        Route::get('pemesanan','Admin\PemesananController@index')->name('admin.pemesanan');
        Route::post('pemesanan/store','Admin\PemesananController@store')->name('admin.pemesanan.store');
        Route::get('pemesanan/view/{id}','Admin\PemesananController@view')->name('admin.pemesanan.view');
        Route::post('pemesanan/update/{id}','Admin\PemesananController@update')->name('admin.pemesanan.update');
        Route::get('pemesanan/finish/{id}','Admin\PemesananController@finish')->name('admin.pemesanan.finish');
    });
    //Client
    //Dashboard
    Route::get('/dashboard','DashboardController@index')->name('dashboard');
    Route::get('/dashboard/create/activity/{id}','DashboardController@create')->name('dashboard.create.activity');
    Route::post('/dashboard/store','DashboardController@store')->name('dashboard.store.activity');
    Route::get('/dashboard/batch/{id}','DashboardController@batch')->name('dashboard.batch');
    //Chips
    Route::get('/chips','ChipsController@index')->name('chips');
    Route::get('/chips/{id}','ChipsController@create')->name('chips.detail');
    Route::post('/chips/{id}','ChipsController@update')->name('chips.update');
    Route::get('/chips/accept/{id}','ChipsController@accept')->name('chips.accept');
    Route::get('/cfilter','ChipsController@filter')->name('chips.filter.date');

    //Media Tanam
    Route::get('/mt','MediaTanamController@index')->name('media_tanam');
    Route::get('/mt/{id}','MediaTanamController@create')->name('media_tanam.detail');
    Route::post('/mt/{id}','MediaTanamController@update')->name('media_tanam.update');
    Route::get('/mfilter','MediaTanamController@filter')->name('media_tanam.filter.date');

    //Obat
    Route::get('/obat','ObatController@index')->name('obat');
    Route::get('/obat/{id}','ObatController@create')->name('obat.detail');
    Route::post('/obat/{id}','ObatController@update')->name('obat.update');
    Route::get('/ofilter','ObatController@filter')->name('obat.filter.date');

    //Karung
    Route::get('/karung','KarungController@index')->name('karung');
    Route::get('/karung/{id}','KarungController@create')->name('karung.detail');
    Route::post('/karung/{id}','KarungController@update')->name('karung.update');
    Route::get('/kfilter','karungController@filter')->name('karung.filter.date');

    //Benang
    Route::get('/benang','BenangController@index')->name('benang');
    Route::get('/benang/{id}','BenangController@create')->name('benang.detail');
    Route::post('/benang/{id}','BenangController@update')->name('benang.update');
    Route::get('/bfilter','BenangController@filter')->name('benang.filter.date');

    //Pemesanan
    Route::get('/pemesanan','PemesananController@index')->name('pemesanan');
    Route::get('/pemesanan/view/{id}','PemesananController@show')->name('pemesanan.view');
    Route::post('/pemesanan/accept/{id}','PemesananController@accept')->name('pemesanan.terima');
    


    //Pengiriman
    Route::get('/pengiriman','PengirimanController@index')->name('pengiriman');
    Route::post('/pengiriman/send/','PengirimanController@store')->name('pengiriman.store');
    Route::post('/pengiriman/jade','PengirimanController@storeJade')->name('pengiriman.store.jade');
});