<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benangs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('terima')->nullable();
            $table->integer('jumlah')->nullable();
            $table->text('desc')->nullable();
            $table->integer('stock')->nullable();
            $table->integer('user_id');
            $table->date('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benangs');
    }
}
