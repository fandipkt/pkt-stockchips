<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTanamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_tanams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('terima')->nullable();
            $table->integer('masuk_kolam')->nullable();
            $table->integer('jumlah')->nullable();
            $table->text('desc')->nullable();
            $table->integer('stock');
            $table->date('created_at');
            $table->integer('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_tanams');
    }
}
