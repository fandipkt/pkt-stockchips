<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produksis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('produksi');
            $table->integer('keluar_jade')->nullable();
            $table->integer('keluar_gaperta')->nullable();
            $table->integer('kembali_jade')->nullable();
            $table->integer('terima_griya')->nullable();
            $table->integer('terima_kim')->nullable();
            $table->integer('kembali_griya')->nullable();
            $table->integer('keluar_gaperta_jade')->nullable();
            $table->integer('keluar_penangkutan')->nullable();
            $table->string('nama_pt')->nullable();
            $table->integer('stock');
            $table->integer('user_id');
            $table->date('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produksis');
    }
}
