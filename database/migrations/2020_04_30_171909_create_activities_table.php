<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sak_masuk')->nullable();
            $table->integer('kurang')->nullable();
            $table->integer('obat_masuk')->nullable();
            $table->integer('produksi_karung')->nullable();
            $table->integer('penggunaan_karung')->nullable();
            $table->integer('batch_id');
            $table->integer('pool_id');
            $table->date('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
