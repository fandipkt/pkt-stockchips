<?php

use Illuminate\Database\Seeder;

class BatchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('batches')->insert([
            'batch' => '1',
            'active'    => 'Y',
            'user_id' => 2,
            'pool_id' => 1,
            'created_at' => date('Y-m-d'),
        ]);
    }
}
