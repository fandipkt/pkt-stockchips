<?php

use Illuminate\Database\Seeder;

class PoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pools')->insert([
            'pool_name' => 'Kolam Depan',
            'pool_capacity'    => 175,
            'user_id' => 2,
            'created_at' => date('Y-m-d h:i:s'),
        ]);
        DB::table('pools')->insert([
            'pool_name' => 'Kolam Belakang',
            'pool_capacity'    => 140,
            'user_id' => 2,
            'created_at' => date('Y-m-d h:i:s'),
        ]);
        DB::table('pools')->insert([
            'pool_name' => 'Kolam Terpal 03 D 20',
            'pool_capacity'    => 140,
            'user_id' => 3,
            'created_at' => date('Y-m-d h:i:s'),
        ]);
        DB::table('pools')->insert([
            'pool_name' => 'Kolam Terpal 04 D 20',
            'pool_capacity'    => 140,
            'user_id' => 3,
            'created_at' => date('Y-m-d h:i:s'),
        ]);
        DB::table('pools')->insert([
            'pool_name' => 'Kolam Terpal 05 D 20',
            'pool_capacity'    => 140,
            'user_id' => 3,
            'created_at' => date('Y-m-d h:i:s'),
        ]);
        DB::table('pools')->insert([
            'pool_name' => 'Kolam Terpal 06 D 20',
            'pool_capacity'    => 140,
            'user_id' => 3,
            'created_at' => date('Y-m-d h:i:s'),
        ]);

        DB::table('pools')->insert([
            'pool_name' => 'Kolam Terpal 07 D 20',
            'pool_capacity'    => 140,
            'user_id' => 3,
            'created_at' => date('Y-m-d h:i:s'),
        ]);

        DB::table('pools')->insert([
            'pool_name' => 'Kolam Terpal 08 D 20',
            'pool_capacity'    => 140,
            'user_id' => 3,
            'created_at' => date('Y-m-d h:i:s'),
        ]);
        DB::table('pools')->insert([
            'pool_name' => 'Kolam Terpal 09 D 20',
            'pool_capacity'    => 140,
            'user_id' => 3,
            'created_at' => date('Y-m-d h:i:s'),
        ]);
        DB::table('pools')->insert([
            'pool_name' => 'Kolam Terpal 10 D 20',
            'pool_capacity'    => 140,
            'user_id' => 3,
            'created_at' => date('Y-m-d h:i:s'),
        ]);
        DB::table('pools')->insert([
            'pool_name' => 'Kolam Terpal 11 D 20',
            'pool_capacity'    => 140,
            'user_id' => 3,
            'created_at' => date('Y-m-d h:i:s'),
        ]);

        DB::table('pools')->insert([
            'pool_name' => 'Kolam Terpal 12 D 20',
            'pool_capacity'    => 140,
            'user_id' => 3,
            'created_at' => date('Y-m-d h:i:s'),
        ]);

        DB::table('pools')->insert([
            'pool_name' => 'Kolam Terpal 13 D 20',
            'pool_capacity'    => 140,
            'user_id' => 3,
            'created_at' => date('Y-m-d h:i:s'),
        ]);

        DB::table('pools')->insert([
            'pool_name' => 'Kolam Terpal 01 Jade 2',
            'pool_capacity'    => 140,
            'user_id' => 4,
            'created_at' => date('Y-m-d h:i:s'),
        ]);

        DB::table('pools')->insert([
            'pool_name' => 'Kolam Terpal 02 Jade 2',
            'pool_capacity'    => 140,
            'user_id' => 4,
            'created_at' => date('Y-m-d h:i:s'),
        ]);

    }
}
