<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'adminpkt',
            'lokasi'    => 'ADMIN',
            'password' => Hash::make('adminpkt123'),
            'role'      => 1,
            'remember_token' => Str::random(10),
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);

        DB::table('users')->insert([
            'username' => 'lokasigriya',
            'lokasi'    => 'GRIYA',
            'password' => Hash::make('lokasigriya'),
            'role'      => 2,
            'remember_token' => Str::random(10),
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('users')->insert([
            'username' => 'lokasikim',
            'lokasi'    => 'KIM',
            'password' => Hash::make('lokasikim'),
            'role'      => 2,
            'remember_token' => Str::random(10),
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);

        DB::table('users')->insert([
            'username' => 'lokasijade',
            'lokasi'    => 'JADE',
            'password' => Hash::make('lokasijade'),
            'role'      => 3,
            'remember_token' => Str::random(10),
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
    }
}
